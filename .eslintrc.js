module.exports = {
  parser: '@typescript-eslint/parser',
  extends: [
    'airbnb-base',
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended',
  ],
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
        paths: ['src'], // Assuming 'src' is your root directory
      },
    },
  },
  parserOptions: {
    ecmaVersion: 2021,
    sourceType: 'module',
    requireConfigFile: false,
  },
  plugins: ['@typescript-eslint', 'react'],
  rules: {
    'no-shadow': 'off',
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: true,
        optionalDependencies: false,
        peerDependencies: false,
      },
    ],
    'import/extensions': [1, { extensions: ['.js', '.jsx', '.ts', '.tsx'] }],
    'max-lines-per-function': ['error', { max: 250 }],
    // '@typescript-eslint/no-magic-numbers': [
    //   'warn',
    //   {
    //     ignoreArrayIndexes: true,
    //     ignore: [0, 1],
    //   },
    // ],
    // 'no-magic-strings': ['error', { allow: ['example', 'test'] }],
  },
};
