import React from 'react';
import WinnersPagination from '../pagination';

type Props = {
  winnersNumber: number;
  winnersPage: number;
  totalPages: number;
};

const WinnersCounter = (props: Props) => {
  const { winnersNumber, winnersPage, totalPages } = props;

  return (
    <div className='winners-footer'>
      <div className='winners-footer__pagination'>
        <WinnersPagination
          winnersNumber={winnersNumber}
          winnersPage={winnersPage}
          totalPages={totalPages}
        />
      </div>
    </div>
  );
};

export default WinnersCounter;
