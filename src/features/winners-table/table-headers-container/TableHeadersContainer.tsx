import React from 'react';
import './style.scss';
import { useAppDispatch } from '../../../store/hooks/hooks';
import {
  setTableSortParam,
  TableSortType,
  toggleTableTimeOrder,
  toggleTableWinsOrder,
  WinnerSortOrder,
  WinnerSortParam,
} from '../../../store/reducers/WinnersSlice';
import TableHeader from '../table-header/TableHeader';

type Props = {
  winnersTable: TableSortType;
};

const TableHeadersContainer = ({ winnersTable }: Props) => {
  const { winsOrder, timeOrder } = winnersTable;
  const dispatch = useAppDispatch();

  const sortWins = () => {
    dispatch(setTableSortParam(WinnerSortParam.WINS));
    dispatch(toggleTableWinsOrder(winsOrder));
  };

  const sortTime = () => {
    dispatch(setTableSortParam(WinnerSortParam.TIME));
    dispatch(toggleTableTimeOrder(timeOrder));
  };

  return (
    <thead className='table-head'>
      <tr className='table-row'>
        <TableHeader text='№' />
        <TableHeader text='Car' />
        <TableHeader text='Name' />
        <TableHeader
          text='Wins'
          handler={sortWins}
          classes={`table-sort ${winsOrder === WinnerSortOrder.ASC ? 'table-sort__asc' : 'table-sort__desc'}`}
        />
        <TableHeader
          text='Best Time'
          handler={sortTime}
          classes={`table-sort ${timeOrder === WinnerSortOrder.ASC ? 'table-sort__asc' : 'table-sort__desc'}`}
        />
      </tr>
    </thead>
  );
};

export default TableHeadersContainer;
