import React from 'react';
import { BiLeftArrow, BiRightArrow } from 'react-icons/bi';
import { useAppDispatch } from '../../../store/hooks/hooks';
import { setPageNumber } from '../../../store/reducers/WinnersSlice';
import { WinnersValues } from '../../../shared/api/winners';
import Button from '../../../shared/components/button/Button';
import '../winnersTable.scss';
// import { selectTotalPages } from '../../../pages/garage/garageSlice';

type Props = {
  winnersPage: number;
  winnersNumber: number;
  totalPages: number;
};

const WinnersPagination = (props: Props) => {
  const { winnersNumber, winnersPage, totalPages } = props;
  const dispatch = useAppDispatch();
  const nextPageExist =
    winnersNumber - winnersPage * WinnersValues.PAGE_LIMIT > 0;

  const showNextPage = () => {
    if (nextPageExist) dispatch(setPageNumber(winnersPage + 1));
  };

  const showPrevPage = () => {
    if (winnersPage !== 1) dispatch(setPageNumber(winnersPage - 1));
  };
  return (
    <div className='winners-pagination__container'>
      <Button
        type='button'
        handler={showPrevPage}
        className='pagination-button btn-medium'
        disabled={winnersPage === 1}
        icon={<BiLeftArrow className='pagination-icon' fill='#DEB9FBFF' />}
      />
      <h3>
        PAGE {winnersPage} / {totalPages}
      </h3>
      <Button
        type='button'
        handler={showNextPage}
        className='pagination-button btn-medium'
        disabled={!nextPageExist}
        icon={<BiRightArrow className='pagination-icon' fill='#DEB9FBFF' />}
      />
    </div>
  );
};

export default WinnersPagination;
