import React from 'react';
import { IWinnerInfo, WinnersValues } from '../../../shared/api/winners';
import './style.scss';
import Image from '../../../shared/components/image';

type Props = {
  winnersPage: number;
  winnersCars: IWinnerInfo[];
};

const TableBody = (props: Props) => {
  const { winnersCars, winnersPage } = props;
  const dosens = (winnersPage - 1) * WinnersValues.PAGE_LIMIT;

  return (
    <tbody>
      {winnersCars.map((winner, index) => (
        <tr className='table-row' key={winner.id} id={winner.id.toString()}>
          <td className='table-data number'>{index + 1 + dosens}</td>
          <td className='table-data'>
            <Image color={winner.color} classes='track__img--table' />
          </td>
          <td className='table-data'>{winner.name}</td>
          <td className='table-data'>{winner.wins}</td>
          <td className='table-data'>{winner.time}</td>
        </tr>
      ))}
    </tbody>
  );
};

export default TableBody;
