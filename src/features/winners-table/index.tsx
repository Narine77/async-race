import React from 'react';
import { IWinnerInfo } from '../../shared/api/winners';
import { TableSortType } from '../../store/reducers/WinnersSlice';
import './winnersTable.scss';
import TableHead from './table-headers-container/TableHeadersContainer';
import Body from './table-body/TableBody';

type Props = {
  winnersPage: number;
  winnersCars: IWinnerInfo[];
  winnersTable: TableSortType;
};

const WinnersTable = ({ winnersCars, winnersPage, winnersTable }: Props) => {
  return (
    <table className='table'>
      <TableHead winnersTable={winnersTable} />
      <Body winnersCars={winnersCars} winnersPage={winnersPage} />
    </table>
  );
};

export default WinnersTable;
