import React, { useEffect } from 'react';
import './style.scss';
import {
  fetchUpdateWinnersTable,
  selectRaceWinner,
} from '../../store/reducers/WinnersSlice';
import { RaceStatus, selectRaceStatus } from '../../store/reducers/GarageSlice';
import { useAppDispatch, useAppSelector } from '../../store/hooks/hooks';

const WinnerPopoup = () => {
  const dispatch = useAppDispatch();
  const raceWinner = useAppSelector(selectRaceWinner);
  const raceStatus = useAppSelector(selectRaceStatus);

  const { winnerId, winnerTime, winnerName } = raceWinner;
  const timeSec = Number(winnerTime.split('ms')[0]) / 1000;

  useEffect(() => {
    if (raceStatus === RaceStatus.PAUSE || raceStatus === RaceStatus.INIT)
      return;
    if (winnerId)
      dispatch(fetchUpdateWinnersTable(+winnerId, +timeSec.toFixed(2)));
  }, [winnerId]);

  return (
    <div
      className={`garage-message ${winnerId ? 'garage-message__show' : 'garage-message__hide'}`}
    >
      <h1>Winner is: {winnerName}</h1>
      <p>Time: {timeSec.toFixed(2)}</p>
    </div>
  );
};

export default WinnerPopoup;
