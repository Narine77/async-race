import React from 'react';
import { useDispatch } from 'react-redux';
import { BiReset } from 'react-icons/bi';
import {
  clearRacersAnimation,
  RaceStatus,
  selectRaceStatus,
  setRaceStatus,
} from '../../../store/reducers/GarageSlice';
import Button from '../../../shared/components/button/Button';
import { useAppSelector } from '../../../store/hooks/hooks';
import { resetRaceWinner } from '../../../store/reducers/WinnersSlice';

const ResetRace = () => {
  const dispatch = useDispatch();
  const raceStatus = useAppSelector(selectRaceStatus);

  const reset = async () => {
    dispatch(setRaceStatus(RaceStatus.END));
    dispatch(clearRacersAnimation());
    dispatch(resetRaceWinner());
  };

  return (
    <Button
      disabled={raceStatus === RaceStatus.END || raceStatus === RaceStatus.INIT}
      text='Reset'
      handler={reset}
      className='button__race btn-medium btn-purple'
      icon={<BiReset />}
    />
  );
};

export default ResetRace;
