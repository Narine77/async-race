import React from 'react';
import {
  fetchCreateCar,
  selectCreatedCar,
  setCreatedCarColor,
  setCreatedCarName,
} from '../../../store/reducers/GarageSlice';
import Button from '../../../shared/components/button/Button';
import { useAppDispatch, useAppSelector } from '../../../store/hooks/hooks';

type Props = {
  pageNumber: number;
};

const CreateCar = ({ pageNumber }: Props) => {
  const createdCar = useAppSelector(selectCreatedCar);
  const dispatch = useAppDispatch();

  const createCar = async () => {
    dispatch(
      fetchCreateCar(
        createdCar.name || 'New Car',
        createdCar.color || '#000000',
        pageNumber,
      ),
    );
    dispatch(setCreatedCarName(''));
  };

  const changeCarName = (event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(setCreatedCarName(event.target.value));
  };

  const changeCarColor = (event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(setCreatedCarColor(event.target.value));
  };

  return (
    <form className='controls__create create-block'>
      <input
        type='text'
        maxLength={25}
        value={createdCar.name}
        placeholder='Car Name'
        onChange={(e) => changeCarName(e)}
        className='controls__input'
      />
      <input
        type='color'
        value={createdCar.color}
        onChange={(e) => changeCarColor(e)}
        className='controls__input color-picker'
      />
      <Button
        text='Create car'
        handler={createCar}
        className='button__create btn-medium btn-purple'
      />
    </form>
  );
};

export default CreateCar;
