import React from 'react';
import { useDispatch } from 'react-redux';
import { IoPlayOutline } from 'react-icons/io5';
import { useAppSelector } from '../../../store/hooks/hooks';
import {
  RaceStatus,
  selectRaceStatus,
  setRaceStatus,
} from '../../../store/reducers/GarageSlice';
import Button from '../../../shared/components/button/Button';

const StartRace = () => {
  const dispatch = useDispatch();
  const raceStatus = useAppSelector(selectRaceStatus);

  const start = async () => {
    dispatch(setRaceStatus(RaceStatus.START));
  };

  return (
    <Button
      disabled={
        raceStatus === RaceStatus.START || raceStatus === RaceStatus.PAUSE
      }
      text='Race'
      handler={start}
      className='button__race btn-medium btn-green'
      icon={<IoPlayOutline />}
    />
  );
};

export default StartRace;
