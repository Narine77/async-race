import React from 'react';
import CreateCar from './create-car';
import UpdateCar from './update-car';
import StartRace from './start-race';
import ResetRace from './reset-race';
import GenerateCars from './generate-cars';
import './race-controls.scss';

type Props = {
  pageNumber: number;
};

const RaceControls = ({ pageNumber }: Props) => (
  <div id='garagePage__button-block'>
    <div className='race-block'>
      <StartRace />
      <ResetRace />
    </div>
    <div className='create-update__container'>
      <CreateCar pageNumber={pageNumber} />
      <UpdateCar pageNumber={pageNumber} />
    </div>
    <div className='controls__race'>
      <GenerateCars />
    </div>
  </div>
);

export default RaceControls;
