import React from 'react';
import { useAppDispatch, useAppSelector } from '../../../store/hooks/hooks';
import {
  fetchGenerateCars,
  selectPageNumber,
} from '../../../store/reducers/GarageSlice';
import Button from '../../../shared/components/button/Button';

const GenerateCars = () => {
  const dispatch = useAppDispatch();
  const currPage = useAppSelector(selectPageNumber);

  const generateCars = async () => {
    dispatch(fetchGenerateCars(currPage));
  };

  return (
    <Button
      text='Generate Cars'
      handler={generateCars}
      className='button__generate btn-medium btn-green'
    />
  );
};

export default GenerateCars;
