import React, { ButtonHTMLAttributes } from 'react';

interface ArrowProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  className?: string;
  fill: string;
  stroke: string;
}

const Arrows: React.FC<ArrowProps> = ({ className = '', stroke, fill }) => {
  return (
    <div className={`arrows ${className}`}>
      <svg
        xmlns='http://www.w3.org/2000/svg'
        width='70'
        height='70'
        viewBox='0 0 34 34'
        fill={fill}
        stroke={stroke}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
        className='feather feather-chevrons-right'
      >
        <polyline points='13 17 18 12 13 7'></polyline>
        <polyline points='6 17 11 12 6 7'></polyline>
      </svg>
    </div>
  );
};

export default Arrows;
