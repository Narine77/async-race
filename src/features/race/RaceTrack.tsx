import React from 'react';
import { ICarData } from '../../shared/api/cars';
import Racer from './racer';
import './raceTrack.scss';

type Props = {
  carsData: ICarData[];
  pageNumber: number;
};

const Racetrack = ({ carsData, pageNumber }: Props) => {
  console.log(carsData, 'carsData');
  return (
    <ul className='garage__track track'>
      {carsData.length > 0 ? (
        carsData.map((carData) => (
          <Racer
            key={carData.id}
            {...{
              carsData,
              carData,
              pageNumber,
            }}
          />
        ))
      ) : (
        <div className='server-not-found'>
          <h4>Server not found</h4>
          <p>
            Please run server, or download on
            <a
              href='https://github.com/mikhama/async-race-api'
              target='_blank'
              rel='noreferrer'
            >
              Github
            </a>
          </p>
        </div>
      )}
    </ul>
  );
};

export default Racetrack;
