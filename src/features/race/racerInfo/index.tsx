import React from 'react';
import { AiFillDelete, AiFillEdit } from 'react-icons/ai';
import { ICarData } from '../../../shared/api/cars';
import {
  fetchDeleteCar,
  removeSelectedCar,
  SelectedCar,
  setSelectedCar,
} from '../../../store/reducers/GarageSlice';

import './style.scss';

import { useAppDispatch } from '../../../store/hooks/hooks';
import Button from '../../../shared/components/button/Button';

type Props = {
  carData: ICarData;
  selectedCar: SelectedCar;
  pageNumber: number;
};

const RacerInfo = ({ carData, selectedCar, pageNumber }: Props) => {
  const dispatch = useAppDispatch();

  const deleteRacer = async () => {
    if (selectedCar.id === carData.id) dispatch(removeSelectedCar());
    dispatch(fetchDeleteCar(carData.id, pageNumber));
  };

  const selectCar = () => {
    const carAlreadySelected = selectedCar.id === carData.id;
    return carAlreadySelected
      ? dispatch(removeSelectedCar())
      : dispatch(
          setSelectedCar({
            id: carData.id,
            color: carData.color,
            name: carData.name,
          }),
        );
  };

  return (
    <div className='track__info info'>
      <div className='info__wrapper'>
        <div className='info__button-wrapper'>
          <Button
            className={
              selectedCar.id === carData.id
                ? 'info__button info__button--active'
                : 'info__button btn-small btn-aqua'
            }
            type='button'
            handler={selectCar}
            text='Edit'
            icon={<AiFillEdit className='info__icon' fill='#fff' />}
          />
        </div>

        <div className='info__button-wrapper'>
          <Button
            className='info__button info__button--delete btn-small btn-purple'
            type='button'
            handler={deleteRacer}
            text='Delete'
            icon={<AiFillDelete className='info__icon' fill='#fff' />}
          />
          {/* <button */}
          {/*  className='info__button info__button--delete' */}
          {/*  type='button' */}
          {/*  onClick={deleteRacer} */}
          {/* > */}
          {/*  <AiFillDelete className='info__icon' fill='#fff' /> */}
          {/*  Delete */}
          {/* </button> */}
        </div>
      </div>
    </div>
  );
};

export default RacerInfo;
