import React from 'react';
import './raceCounter.scss';
import Pagination from './pagination/Pagination';

type Props = {
  carsNumber: number;
};

const RaceCounter = ({ carsNumber }: Props) => {
  return (
    <div className='garage-footer'>
      <h3>GARAGE ( {carsNumber} )</h3>
      <div className='garage-footer__pagination'>
        <Pagination />
      </div>
    </div>
  );
};

export default RaceCounter;
