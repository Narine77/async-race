import React from 'react';
import { BiLeftArrow, BiRightArrow } from 'react-icons/bi';

import {
  clearRacersAnimation,
  fetchCurrentPageCars,
  RaceStatus,
  selectPageNumber,
  selectTotalCars,
  selectTotalPages,
  setPageNumber,
  setRaceStatus,
} from '../../../../store/reducers/GarageSlice';
import { useAppDispatch, useAppSelector } from '../../../../store/hooks/hooks';
import { resetRaceWinner } from '../../../../store/reducers/WinnersSlice';
import { GarageValues } from '../../../../shared/api/cars';
import Button from '../../../../shared/components/button/Button';

const Pagination = () => {
  const dispatch = useAppDispatch();
  const carsNumber = useAppSelector(selectTotalCars);
  const pageNumber = useAppSelector(selectPageNumber);
  const nextPageExist = carsNumber - pageNumber * GarageValues.PAGE_LIMIT > 0;
  const totalPages = useAppSelector(selectTotalPages);
  const resetRace = () => {
    dispatch(setRaceStatus(RaceStatus.INIT));
    dispatch(clearRacersAnimation());
    dispatch(resetRaceWinner());
  };

  const showNextPage = async () => {
    if (nextPageExist) {
      dispatch(setPageNumber(pageNumber + 1));
      dispatch(fetchCurrentPageCars(pageNumber + 1));
      resetRace();
    }
  };

  const showPrevPage = async () => {
    if (pageNumber !== 1) {
      dispatch(setPageNumber(pageNumber - 1));
      dispatch(fetchCurrentPageCars(pageNumber - 1));
      resetRace();
    }
  };

  return (
    <div className='garage-pagination__container'>
      <Button
        type='button'
        handler={showPrevPage}
        className='btn-medium'
        disabled={pageNumber === 1}
        icon={<BiLeftArrow className='pagination-icon' fill='#DEB9FBFF' />}
      />
      <h3>
        PAGE {pageNumber} / {totalPages}
      </h3>
      <Button
        type='button'
        handler={showNextPage}
        className='btn-medium'
        disabled={!nextPageExist}
        icon={<BiRightArrow className='pagination-icon' fill='#DEB9FBFF' />}
      />
    </div>
  );
};

export default Pagination;
