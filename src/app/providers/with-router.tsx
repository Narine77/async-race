import React, { Suspense } from 'react';
import { HashRouter as Router } from 'react-router-dom';
import Preloader from '../../shared/components/preloader';

const withRouter = (component: () => React.ReactNode) => () => (
  <Router>
    <Suspense fallback={<Preloader />}>{component()}</Suspense>
  </Router>
);

export default withRouter;
