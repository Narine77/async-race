import React from 'react';
import withProviders from './providers';
import Routing from '../pages';
import Wrapper from '../widgets/wrapper/Wrapper';

const App: React.FC = () => {
  return (
    <div className='content'>
      <Wrapper>
        <Routing />
      </Wrapper>
    </div>
  );
};

export default withProviders(App);
