import React from 'react';
import './navbar.scss';
import { Link, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
// import Button from '../form/Button';
import Arrows from '../../features/arrows/Arrows';
import { useAppSelector } from '../../store/hooks/hooks';
import {
  RaceStatus,
  selectRaceStatus,
  setRaceStatus,
} from '../../store/reducers/GarageSlice';
import { PageName } from '../../store/appSlice';
import Button from '../../shared/components/button/Button';

const NavBar: React.FC = () => {
  const numberOfSvg = 5;
  const dispatch = useDispatch();
  const raceStatus = useAppSelector(selectRaceStatus);
  const location = useLocation();

  const switchPage = () => {
    if (raceStatus === RaceStatus.START)
      dispatch(setRaceStatus(RaceStatus.PAUSE));
    if (raceStatus === RaceStatus.END) dispatch(setRaceStatus(RaceStatus.INIT));
  };

  return (
    <div id='navbar'>
      <div className='container'>
        <div className='navbar__button-block'>
          <li
            role='menuitem'
            className='header__item'
            onClick={switchPage}
            onKeyDown={switchPage}
          >
            <Link
              to='/'
              className={
                location.pathname === '/'
                  ? 'header__link header__link--active'
                  : 'header__link'
              }
            >
              <Button text={PageName.GARAGE} className='btn-big btn-aqua' />
            </Link>
          </li>
          <li
            role='menuitem'
            className='header__item'
            onClick={switchPage}
            onKeyDown={switchPage}
          >
            <Link
              to='/winners'
              className={
                location.pathname === '/winners'
                  ? 'header__link header__link--active'
                  : 'header__link'
              }
            >
              <Button text={PageName.WINNERS} className='btn-big btn-purple' />
            </Link>
            {/* <Link to='/'> */}
            {/*  <Button title='GARAGE' className='btn-big btn-aqua' /> */}
            {/* </Link> */}
            {/* <Link to='/winners'> */}
            {/*  <Button title='WINNERS' className='btn-big btn-purple' /> */}
            {/* </Link> */}
          </li>
        </div>
        <div className='navbar__title-block'>
          <div className='navbar-title-block__purple-arrows'>
            {Array.from({ length: numberOfSvg }, (_, index) => (
              <Arrows
                key={index}
                className='purple'
                fill='none'
                stroke='#DEB9FBFF'
              />
            ))}
          </div>
          <div className='logo'>
            <div className='logo-purple-line'>
              <div className='logo-aqua-line'>
                <p> ASYNC RACE</p>
              </div>
            </div>
          </div>
          <div className='navbar-title-block__aqua-arrows'>
            {Array.from({ length: numberOfSvg }, (_, index) => (
              <Arrows
                key={index}
                className='purple'
                fill='none'
                stroke='#8ed8d8'
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default NavBar;
