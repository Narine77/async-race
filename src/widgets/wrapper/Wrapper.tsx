import React from 'react';
import NavBar from '../navbar/NavBar';
import Footer from '../footer/Footer';

interface Props {
  children: React.ReactNode;
}

const Wrapper = ({ children }: Props) => {
  return (
    <div id='wrapper'>
      <NavBar />
      {children}
      <Footer />
    </div>
  );
};

export default Wrapper;
