import React, { useEffect } from 'react';
import WinnersCounter from '../../features/winners-table/counter';
import WinnersTable from '../../features/winners-table';
import Preloader from '../../shared/components/preloader';
import { selectLoading } from '../../store/appSlice';
import { useAppDispatch, useAppSelector } from '../../store/hooks/hooks';
import {
  fetchGetWinners,
  selectPageNumber,
  selectTableSort,
  selectTotalPages,
  selectTotalWinners,
  selectWinnersCars,
  WinnerSortParam,
} from '../../store/reducers/WinnersSlice';
import '../style.css';

const Winners = () => {
  const dispatch = useAppDispatch();
  const winnersCars = useAppSelector(selectWinnersCars);
  const winnersPage = useAppSelector(selectPageNumber);
  const winnersNumber = useAppSelector(selectTotalWinners);
  const totalPages = useAppSelector(selectTotalPages);
  const tableSort = useAppSelector(selectTableSort);
  const loading = useAppSelector(selectLoading);

  useEffect(() => {
    const currOrder =
      tableSort.sort === WinnerSortParam.WINS
        ? tableSort.winsOrder
        : tableSort.timeOrder;
    dispatch(fetchGetWinners(winnersPage, tableSort.sort, currOrder));
  }, [tableSort.sort, tableSort.timeOrder, tableSort.winsOrder, winnersPage]);

  return (
    <div id='winners'>
      <div className='container'>
        {loading && <Preloader />}
        <h2>Winners: {winnersNumber}</h2>
        <WinnersTable
          winnersCars={winnersCars}
          winnersPage={winnersPage}
          winnersTable={tableSort}
        />
        <WinnersCounter
          winnersNumber={winnersNumber}
          winnersPage={winnersPage}
          totalPages={totalPages}
        />
      </div>
    </div>
  );
};

export default Winners;
