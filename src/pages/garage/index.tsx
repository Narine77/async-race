import React, { useEffect } from 'react';

import Racetrack from '../../features/race/RaceTrack';
import WinMessage from '../../features/winner-popup';
import RaceControls from '../../features/race-controls/RaceControls';
import RaceCounter from '../../features/race/race-counter/RaceCounter';
import Preloader from '../../shared/components/preloader';

import { selectLoading } from '../../store/appSlice';
import {
  fetchCurrentPageCars,
  selectCurrentCars,
  selectPageNumber,
  selectTotalCars,
} from '../../store/reducers/GarageSlice';
import { useAppDispatch, useAppSelector } from '../../store/hooks/hooks';
import '../style.css';
import RaceLines from '../../shared/components/race-lines/RaceLines';

const Garage = () => {
  const dispatch = useAppDispatch();
  const carsData = useAppSelector(selectCurrentCars);
  const pageNumber = useAppSelector(selectPageNumber);
  const carsNumber = useAppSelector(selectTotalCars);
  const loading = useAppSelector(selectLoading);

  useEffect(() => {}, [carsData, pageNumber]);

  useEffect(() => {
    dispatch(fetchCurrentPageCars(pageNumber));
  }, []);

  return (
    <div id='garage'>
      <div className='container'>
        {loading && <Preloader />}
        <WinMessage />
        <RaceControls pageNumber={pageNumber} />
        <RaceLines />
        <Racetrack carsData={carsData} pageNumber={pageNumber} />
        <RaceLines />
        <RaceCounter carsNumber={carsNumber} />
      </div>
    </div>
  );
};

export default Garage;
