import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from './hooks';

interface AppState {
  loading: boolean;
  page: string;
}

export enum PageName {
  GARAGE = 'Garage',
  WINNERS = 'Winners',
}

const initialState: AppState = {
  loading: false,
  page: PageName.GARAGE,
};

export const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setLoading: (state) => {
      return { ...state, loading: true };
    },
    removeLoading: (state) => {
      return { ...state, loading: false };
    },
    setCurrPage: (state, action: PayloadAction<PageName>) => {
      return { ...state, page: action.payload };
    },
  },
});

export const { setLoading, removeLoading, setCurrPage } = appSlice.actions;

export const selectLoading = (state: RootState) => state.app.loading;
export const selectCurrPage = (state: RootState) => state.app.page;

export default appSlice.reducer;
