import axios, { AxiosError } from 'axios';
import { apiInstance, UrlPath } from './base';

export enum EngineStatus {
  START = 'started',
  STOP = 'stopped',
  DRIVE = 'drive',
}

export interface IEngine {
  velocity: number;
  distance: number;
}

export interface IEngineDrive {
  success: boolean;
}

export const engineAPI = {
  async drive(id: number): Promise<boolean> {
    try {
      const response = await apiInstance.patch<boolean>(
        `${UrlPath.ENGINE}/?id=${id}&status=${EngineStatus.DRIVE}`,
      );

      return response.data;
    } catch (err) {
      if (axios.isAxiosError(err)) {
        const axiosErr = err as AxiosError; // Cast err to AxiosError
        if (axiosErr.response?.status === 500) {
          console.error('Car engine has broken down due to overheating');
          return false;
        }
        // Handle other Axios errors if needed
      }

      console.error('Unexpected error:', err);
      throw new Error('Failed to drive the car');
    }
  },
  async start(id: number) {
    return apiInstance
      .patch<IEngine>(
        `${UrlPath.ENGINE}/?id=${id}&status=${EngineStatus.START}`,
      )
      .then((response) => response.data)
      .catch((err) => {
        throw new Error(err);
      });
  },
  async stop(id: number) {
    return apiInstance
      .patch<IEngine>(`${UrlPath.ENGINE}/?id=${id}&status=${EngineStatus.STOP}`)
      .then((response) => response.data)
      .catch((err) => {
        throw new Error(err);
      });
  },
};
