import axios from 'axios';

export enum UrlPath {
  BASE = 'http://localhost:3000',
  GARAGE = 'garage',
  WINNERS = 'winners',
  ENGINE = 'engine',
}

export const apiInstance = axios.create({
  baseURL: UrlPath.BASE,
});
