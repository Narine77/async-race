import React from 'react';

const Preloader = () => (
  <div className='wrapper'>
    <div className='lds-spinner'>
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
    </div>
  </div>
);

export default Preloader;
