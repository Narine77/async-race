import React, { ButtonHTMLAttributes } from 'react';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  text?: string;
  classes?: string;
  handler?: () => void;
  disabled?: boolean;
  id?: string;
  children?: React.ReactElement[] | React.ReactElement;
  icon?: React.ReactNode;
}
const Button: React.FC<ButtonProps> = ({
  id,
  text,
  className,
  handler,
  disabled,
  children,
  icon,
}) => {
  return (
    <button
      id={id}
      disabled={disabled}
      className={`btn ${className} ${disabled ? 'button--disabled' : ''}`}
      type='button'
      onClick={handler}
    >
      {children}
      {text}
      {icon || null}
    </button>
  );
};

export default Button;
