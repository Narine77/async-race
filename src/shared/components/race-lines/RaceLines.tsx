import React from 'react';
import './raceLines.css';

const RaceLines = () => {
  return (
    <div className='race-edge'>
      <div className='race-edge__inner'></div>
    </div>
  );
};

export default RaceLines;
